import argparse
import csv
import sys

from api.calculator import (
    Bond,
    SpreadToBenchmark,
    SpreadToCurve
)


CALCULATORS = {
    'spread_to_benchmark': SpreadToBenchmark,
    'spread_to_curve': SpreadToCurve
}


def valid_calculator(value):
    if value not in CALCULATORS.keys():
        raise argparse.ArgumentTypeError(
            'invalid calculator. Choices include %s' % ', '.join(CALCULATORS.keys()))
    return value


def parse_args(args):
    parser = argparse.ArgumentParser(
        description='Corporate Bond yield spread calculator')

    parser.add_argument('calculator_method',
                        type=valid_calculator,
                        help='method to calculate the spread')

    parser.add_argument('input_csv',
                        help='''Path of the input csv file. Must contain the following columns:
                        bond,
                        type,
                        term,
                        yield.
                        term must be in years.''')

    return parser.parse_args(args)


def read_csv(input_file):
    government_bonds = []
    corporate_bonds = []
    with open(input_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            bond = Bond(
                row['bond'].strip(),
                row['type'].strip(),
                float(row['term'].strip().split(' ')[0]),
                float(row['yield'].strip('%').strip())
            )

            bond_type = bond.type.lower()
            if bond_type == 'corporate':
                corporate_bonds.append(bond)
            elif bond_type == 'government':
                government_bonds.append(bond)
            else:
                raise ValueError('Unknown bond type: %s,' % bond.type)
    government_bonds.sort(key=lambda x: x.term)
    return corporate_bonds, government_bonds


def get_calculator_class(calculator_method):
    return CALCULATORS[calculator_method]


def calculate_spread(calculator_class, government_bonds, corporate_bonds):
    calculator = calculator_class()
    for cbond in corporate_bonds:
        yield calculator.calculate(cbond, government_bonds)


def main(args):
    # calculate results
    government_bonds, corporate_bonds = read_csv(args.input_csv)
    calculator_class = get_calculator_class(args.calculator_method)
    results = calculate_spread(
        calculator_class, corporate_bonds, government_bonds)

    # write to stdout
    writer = csv.DictWriter(
        sys.stdout, fieldnames=calculator_class.OUTPUT_FIELDNAMES)
    writer.writeheader()
    for row in results:
        writer.writerow(row)

if __name__ == '__main__':
    parsed_args = parse_args(sys.argv[1:])
    main(parsed_args)
