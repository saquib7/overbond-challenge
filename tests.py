import argparse
import os
from unittest import TestCase

from spread_calculator import (
    CALCULATORS,
    SpreadToBenchmark,
    SpreadToCurve,
    valid_calculator,
    get_calculator_class,
    read_csv,
    parse_args
)

DATA = '''bond,type,term,yield
C1, corporate, 1.3 years,3.30%
C2, corporate, 2.0 years,3.80%
C3, corporate, 5.2 years,5.30%
C4, corporate, 9.0 years,6.20%
C5, corporate, 10.1 years,6.40%
C6, corporate, 16.0 years,9.30%
C7, corporate, 22.9 years,12.30%
G1, government, 0.9 years,1.70%
G2, government, 2.3 years,2.30%
G3, government, 7.8 years,3.30%
G4, government, 12 years,5.50%
G5, government, 15.1 years,7.50%
G6, government, 24.2 years,9.80%
'''


class ValidCalculatorTestCase(TestCase):

    def test_valid_calculator(self):
        for key in CALCULATORS.keys():
            self.assertEqual(valid_calculator(key), key)

    def test_invalid_calculator(self):
        with self.assertRaises(argparse.ArgumentTypeError):
            valid_calculator('invalid_calculator')


class ReadCSVTestCase(TestCase):

    def setUp(self):
        with open('sample_test.csv', 'w') as f:
            f.write(DATA)

    def test_with_valid_csv(self):
        corporate_bonds, government_bonds = read_csv('sample_test.csv')
        self.assertEqual(len(government_bonds), 6)
        self.assertEqual(len(corporate_bonds), 7)

    def tearDown(self):
        os.remove('sample_test.csv')


class GetCalculatorClassTestCase(TestCase):

    def test(self):
        cc = list(CALCULATORS.keys())[0]
        calculator_class = get_calculator_class(cc)
        self.assertEqual(calculator_class, CALCULATORS[cc])


class ParseArgs(TestCase):

    def test_with_valid_calculator(self):
        cc = list(CALCULATORS.keys())[0]
        args = [cc, 'path/to/csv']
        parsed_args = parse_args(args)
        self.assertEqual(parsed_args.calculator_method, cc)
        self.assertEqual(parsed_args.input_csv, 'path/to/csv')
