from unittest import TestCase
from api.helpers import (
    Point,
    LinearInterpolator,
    binary_search
)

class LinearInterpolatorTestCase(TestCase):

    def test_with_points1(self):
        p1 = Point(1, 2)
        p2 = Point(3, 4)
        li = LinearInterpolator(p1, p2)
        self.assertEqual(li.slope, 1)
        self.assertEqual(li.intercept, 1)
        self.assertEqual(li.calculate_y_given_x(5), 6)

    def test_with_points2(self):
        p1 = Point(-1, 2)
        p2 = Point(3, -4)
        li = LinearInterpolator(p1, p2)
        self.assertEqual(li.slope, -1.5)
        self.assertEqual(li.intercept, 0.5)
        self.assertEqual(li.calculate_y_given_x(5), -7.0)


class BinarySearchTestCase(TestCase):

    def setUp(self):
        self.arr = [1, 3, 5, 7, 9]

    def test_when_element_exists(self):
        idx = binary_search(1, self.arr)
        self.assertEqual(idx, 0)

        idx = binary_search(3, self.arr)
        self.assertEqual(idx, 1)

        idx = binary_search(5, self.arr)
        self.assertEqual(idx, 2)

        idx = binary_search(7, self.arr)
        self.assertEqual(idx, 3)

        idx = binary_search(9, self.arr)
        self.assertEqual(idx, 4)

    def test_when_element_does_not_exist(self):
        idx = binary_search(0, self.arr)
        self.assertEqual(idx, 0)

        idx = binary_search(2, self.arr)
        self.assertEqual(idx, 1)

        idx = binary_search(4, self.arr)
        self.assertEqual(idx, 2)

        idx = binary_search(6, self.arr)
        self.assertEqual(idx, 3)

        idx = binary_search(8, self.arr)
        self.assertEqual(idx, 4)

        idx = binary_search(10, self.arr)
        self.assertEqual(idx, 5)

    def test_with_key(self):
        arr = [(1, 2), (3, 4)]

        idx = binary_search(1, arr, key=lambda x: x[0])
        self.assertEqual(idx, 0)

        idx = binary_search(4, arr, key=lambda x: x[0])
        self.assertEqual(idx, 2)

        idx = binary_search(4, arr, key=lambda x: x[1])
        self.assertEqual(idx, 1)
