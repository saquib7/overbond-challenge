from unittest import TestCase

from api.calculator import (
    Point,
    Bond,
    LinearInterpolator,
    SpreadToBenchmark,
    SpreadToCurve
)


class SpreadToBenchmarkTestCase(TestCase):

    def setUp(self):
        self.government_bonds = [
            Bond('G1', 'government', 9.4, 3.7),
            Bond('G2', 'government', 12, 4.8),
        ]

    def test_mid(self):
        cbond = Bond('C1', 'corporate', 10.3, 5.3)
        stb = SpreadToBenchmark()
        result = stb.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_benchmark'), 1.6)

    def test_first(self):
        cbond = Bond('C1', 'corporate', 9, 4.1)
        stb = SpreadToBenchmark()
        result = stb.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_benchmark'), 0.4)

    def test_last(self):
        cbond = Bond('C1', 'corporate', 13, 7.5)
        stb = SpreadToBenchmark()
        result = stb.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_benchmark'), 2.7)
        


class CurveToBenchmarkTestCase(TestCase):

    def setUp(self):
        self.government_bonds = [            
            Bond('G1', 'government', 9.4, 3.70),
            Bond('G2', 'government', 12, 4.80),
            Bond('G3', 'government', 16.3, 5.50),
        ]

    def test_mid(self):
        cbond = Bond('C1', 'corporate', 10.3, 5.30)
        stc = SpreadToCurve()
        result = stc.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_curve'), 1.22)

        cbond = Bond('C2', 'corporate', 15.2, 8.30)
        stc = SpreadToCurve()
        result = stc.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_curve'), 2.98)
        
    def test_first(self):
        cbond = Bond('C1', 'corporate', 9, 4.1)
        stb = SpreadToCurve()
        result = stb.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_curve'), 0.4)

    def test_last(self):
        cbond = Bond('C1', 'corporate', 17, 6.5)
        stb = SpreadToCurve()
        result = stb.calculate(cbond, self.government_bonds)
        self.assertAlmostEqual(result.get('spread_to_curve'), 1.0)
