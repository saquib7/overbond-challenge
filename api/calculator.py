from collections import namedtuple
from api.helpers import (
    Point,
    LinearInterpolator,
    binary_search
)

# the field is called bond_yield instead of yield because
# yield is reserved keyword in python. Though it won't
# cause an error, most editors will complain about it.
Bond = namedtuple('Bond', ['name', 'type', 'term', 'bond_yield'])


class SpreadToBenchmark(object):

    OUTPUT_FIELDNAMES = ['bond', 'benchmark', 'spread_to_benchmark']

    def calculate(self, cbond, government_bonds):
        # find the location in the array where cbond belongs,
        # given it's term
        idx = binary_search(cbond.term, government_bonds, key=lambda x: x.term)
        candidate1 = government_bonds[idx] if idx < len(
            government_bonds) else None
        candidate2 = government_bonds[idx - 1] if idx > 0 else None

        if candidate1 is None:
            ret = candidate2
        elif candidate2 is None:
            ret = candidate1
        else:
            c = min(candidate1, candidate2,
                    key=lambda x: abs(cbond.term - x.term))
            ret = c

        return {
            'bond': cbond.name,
            'benchmark': ret.name,
            'spread_to_benchmark': round(cbond.bond_yield - ret.bond_yield, 2)
        }


class SpreadToCurve(object):

    OUTPUT_FIELDNAMES = ['bond', 'spread_to_curve']

    def calculate(self, cbond, government_bonds):
        # find the location in the array where cbond belongs,
        # given it's term
        idx = binary_search(cbond.term, government_bonds, key=lambda x: x.term)
        candidate1 = government_bonds[idx] if idx < len(
            government_bonds) else None
        candidate2 = government_bonds[idx - 1] if idx > 0 else None

        if candidate1 is None:
            ret = candidate2.bond_yield
        elif candidate2 is None:
            ret = candidate1.bond_yield
        else:
            p1 = Point(candidate1.term, candidate1.bond_yield)
            p2 = Point(candidate2.term, candidate2.bond_yield)
            li = LinearInterpolator(p1, p2)
            ret = li.calculate_y_given_x(cbond.term)

        return {
            'bond': cbond.name,
            'spread_to_curve': round(cbond.bond_yield - ret, 2)
        }
