from collections import namedtuple


Point = namedtuple('Point', ['x', 'y'])


class LinearInterpolator(object):

    def __init__(self, p1, p2):
        self._m = (p1.y - p2.y) / (p1.x - p2.x)
        self._b = p1.y - self._m * p1.x

    def calculate_y_given_x(self, x):
        return self._m * x + self._b

    @property
    def slope(self):
        return self._m

    @property
    def intercept(self):
        return self._b


def binary_search(val, arr, key=None):
    key = (lambda x: x) if key is None else key
    start = 0
    end = len(arr) - 1

    while start <= end:
        mid = (end + start) // 2
        mid_val = key(arr[mid])
        if mid_val == val:
            return mid
        if mid_val < val:
            start = mid + 1
        else:
            end = mid - 1

    # return where the element ought to be
    return start
