## Usage

From within the root project directory,
Run the tests: `python -m unittest discover`
Calculate spread to benchmark with sample data `python spread_calculator.py spread_to_benchmark sample.csv`
Calculate spread to curve with sample data `python spread_calculator.py spread_to_curve sample.csv`

## Approach

Strategy pattern is used to encapsulate the `spread_to_curve` and `spread_to_benchmark` algorithms. 
Both employ a binary search on a sorted `government_bonds` array to find the goverment bonds to compare to each corporate bond. 

## Improvements

The code calling the `SpreadToBenchmark` and `SpreadToCurve` could be encapsulated in a `Calculator` class. This would be a logical step forward and would allow building of caching mechanisms to avoid recalculation. This would also simplify the api to these libraries. The obvious disadvantage is that it violates the YAGNI principle (You Ain't Gonna Need It).

Mocks could be used in tests when the function bieng tested calls other functions that aren't bieng tested. This is important to do in a production system when CI starts taking an unreasonable amount of time. 

The `SpreadToBenchmark` and `SpreadToCurve` both contain a class variable `OUTPUT_FIELDNAMES`. This is not great. I introduced those so that I could specify an order to the output fields when using `csv.DictWriter`, which is awfully convenient to use. I would really like to explain how I would fix this bad design, and am looking forward to doing so in person!
